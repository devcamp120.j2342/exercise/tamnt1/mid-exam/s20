package com.devcamp.stringapi.controllers;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.stringapi.services.StringService;

@RestController
@CrossOrigin
public class MainController {
    @Autowired
    private StringService stringService;

    @GetMapping("/reverse-string")
    public String reverseString(@RequestParam("inputString") String inputString) {
        return stringService.getReverseString(inputString);
    }

    @GetMapping("/check-palindrome")
    public String checkPalindrome(@RequestParam("inputString") String inputString) {
        return stringService.getPalindrome(inputString);
    }

    @GetMapping("/remove-duplicates")
    public String removeDuplicates(@RequestParam("inputString") String inputString) {
        return stringService.getRemoveDuplicates(inputString);
    }

    @GetMapping("/concat-strings")
    public String concatStrings(@RequestParam("string1") String string1, @RequestParam("string2") String string2) {
        return stringService.getConcatStrings(string1, string2);
    }
}
