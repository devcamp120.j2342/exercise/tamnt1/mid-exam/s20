package com.devcamp.stringapi.services;

import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Service;

@Service
public class StringService {
    public String getReverseString(String inputString) {
        String reversedString = new StringBuilder(inputString).reverse().toString();

        return reversedString;
    }

    public String getPalindrome(String inputString) {
        String normalizedString = inputString.replaceAll("\\s+", "").toLowerCase();
        String reversedString = new StringBuilder(normalizedString).reverse().toString();

        if (normalizedString.equals(reversedString)) {
            return "Chuỗi '" + inputString + "' là chuỗi palindrome.";
        } else {
            return "Chuỗi '" + inputString + "' không phải là chuỗi palindrome.";
        }
    }

    public String getRemoveDuplicates(String inputString) {
        StringBuilder result = new StringBuilder();
        Set<Character> seen = new HashSet<>();

        for (char c : inputString.toCharArray()) {
            if (!seen.contains(c)) {
                result.append(c);
                seen.add(c);
            }
        }

        return result.toString();
    }

    public String getConcatStrings(String string1, String string2) {
        int length1 = string1.length();
        int length2 = string2.length();

        if (length1 < length2) {
            string2 = string2.substring(length2 - length1);
        } else if (length1 > length2) {
            string1 = string1.substring(length1 - length2);
        }

        return string1.concat(string2);
    }

}
